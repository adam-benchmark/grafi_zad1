<?php

namespace Services\BeautifyService;

class BeautifyService
{
    const ACCOUNT_PATTERN = '#([0-9]{2})([0-9]{4})([0-9]{4})([0-9]{4})([0-9]{4})([0-9]{4})([0-9]{4})#';

    /**
     * @param string $input
     *
     * @return string
     */
    public function getBeautifyAccount(string $input)
    {
        $outputArray = $this->convertToBeautify($input);
        $beautifyFormat = implode(" ", $outputArray);

        return $beautifyFormat;
    }

    /**
     * @param $input
     *
     * @return mixed
     */
    private function convertToBeautify($input)
    {
        preg_match(self::ACCOUNT_PATTERN, $input, $matches);
        unset($matches[0]);

        return $matches;
    }
}
